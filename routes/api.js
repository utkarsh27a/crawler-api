var express = require('express');
var TigerdirectScraper = require('../Scrapers/TigerdirectScraper');
var router = express.Router();

function sendErrRes(res, message) {
    return res.json({
        message,
        staus: "failed"
    })
}

router.get('/tigerdirect/scrape', async (req, res, next) => {
    let product_url = req.query.product_url;
    try {
        product_url = new URL(product_url);
        if (product_url.host != "www.tigerdirect.com") {
            return sendErrRes(res, "This app can fetch reviews only for www.tigerdirect.com");
        }
        product_url.searchParams.set('pagenumber', 0)
        product_url.searchParams.set('RSort', 1)
        product_url.searchParams.set('recordsPerPage', 5)
        product_url.searchParams.set('body', 'REVIEWS')
    }catch(err) {
        console.log(err);
        return sendErrRes(res, "INVALID URL");
    }

    let inst = new TigerdirectScraper();
    inst.run(product_url)
    .then((reviews) => {
        res.json({ staus: "success", reviews, count: reviews.length });
    })
    .catch((err) => {
        console.log(err);
        return sendErrRes(res, err);
    });
});

module.exports = router;
