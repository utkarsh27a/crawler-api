const fs = require('fs');
const puppeteer = require('puppeteer');

const config = require('../config');

/*
* Define a simple logger
*/
console.mylogger = (str) =>  {
    console.log("======================================");
    console.log('\x1b[36m%s\x1b[0m', str);
    console.log("======================================");
}

console.mylogger("========= Launching Browser ==========");
var browser;
puppeteer.launch({ headless: true })
.then(inst => {
    browser = inst;
});

class BaseScraper {

    constructor() {
        this.pageNumber = config.get("pageNumber");
        this.DATA = [];
    }

    saveData() {
        let file = this.scraperName || "output";
        return new Promise((resolve, reject) => {
            fs.writeFile('../outputs/' + file + '.json', JSON.stringify(this.DATA, null, 4), 'ascii', (err, res) => {
                if (err) {
                    return reject(err);
                }
                return resolve(res);
            });
        });
    }

    async run(product_url) {
        try  {
            this.url = product_url;
            console.mylogger("=========== Open new Tab =============");
            const page = await browser.newPage();


            console.mylogger("=========== Setting size =============");
            await page.setViewport({ width: 1920, height: 926 });

            page.on("response", response => {
                const request = response.request();
                const url = request.url();
                const status = response.status();
                if (url === this.url.href) {
                    this.pageStatus = status;
                }
            });
            console.mylogger(`============= Open URL =============== \n============= Page No. ${this.pageNumber} =============`);
            await page.goto(this.url.href);
            if (this.pageStatus && ([200,201]).indexOf(this.pageStatus) === -1) {
                console.log("page status is ", this.pageStatus);
                return Promise.reject(`Got ${this.pageStatus} response from URL`);
            }

            this.totalReviewsCount = await this.getTotalReviewCount(page);
            console.mylogger(`========= Total Reviews: ${this.totalReviewsCount} ===========`);
            if (!this.totalReviewsCount || this.totalReviewsCount === 0) {
                console.log("page status is ", this.pageStatus);
                return Promise.reject(`We can't find any reviews on this URL.`);
            }

            this.totalPagesCount = Math.ceil(this.totalReviewsCount / this.reviewsPerPage);
            console.mylogger(`========== Total Pages: ${this.totalPagesCount} ===========`);

            console.mylogger("========= Starting Scraper ===========");
            var data = await this.startScraper(page);
            await page.close();
            return data;
        }catch(err) {
            console.log(err);
            return Promise.reject("Something went wrong, please try again.");
        }
    }

    getTotalReviewCount() {
        return Promise.resolve()
    }

    startScraper() {
        return Promise.resolve()
    }

}

module.exports = BaseScraper;
