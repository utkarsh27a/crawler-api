const BaseScraper = require("./BaseScraper");
const config = require('../config');

class TigerdirectScraper extends BaseScraper {

    constructor() {
        super();
        this.scraperName = 'tigerdirect-reviews';
        this.reviewsPerPage = config.get("reviewsPerPage");
    }

    getReviewsOfPage(page) {
        return page.evaluate(() => {
            let reviews = [];
            // get the review elements
            let reviewsElms = document.querySelectorAll('div#customerReviews #customerReviews');
            // get the review data
            reviewsElms.forEach((reviewsElement) => {
                let reviewJson = {};
                try {
                    reviewJson.comment = {
                        heading: reviewsElement.querySelector('.rightCol h6').innerText,
                        text: reviewsElement.querySelector('.rightCol p').innerText
                    };
                    reviewJson.reviewer_name = document.querySelectorAll('div#customerReviews')[0].querySelector('div#customerReviews div.leftCol .reviewer').querySelectorAll('dd')[0].innerText;
                    reviewJson.date = document.querySelectorAll('div#customerReviews')[0].querySelector('div#customerReviews div.leftCol .reviewer').querySelectorAll('dd')[1].innerText;
                    reviewJson.rating = document.querySelectorAll('div#customerReviews')[0].querySelector('div#customerReviews div.leftCol .itemReview').querySelectorAll('dd')[0].innerText;
                }
                catch (exception){
                    console.log(exception);
                }
                reviews.push(reviewJson);
            });
            return reviews;
        });
    }

    getTotalReviewCount(page) {
        return page.evaluate(async () => {
            let element = document.querySelector('.reviewsPagination .reviewPage dt');
            if (!element) {
                return 0;
            }
            let words = element.innerText.split(String.fromCharCode(160));
            return parseInt(words[words.length - 1]);
        });
    }

    async startScraper(page) {
        this.DATA = await this.getReviewsOfPage(page);
        for (var pagenumber = this.pageNumber + 1; pagenumber < this.totalPagesCount; pagenumber++) {
            console.mylogger(`========== Open Page no: ${pagenumber} ===========`);
            this.url.searchParams.set('pagenumber', pagenumber);
            try {
                await page.goto(this.url.href);
                if (this.pageStatus && ([200,201]).indexOf(this.pageStatus) === -1) {
                    console.log("page status is ", this.pageStatus);
                    return Promise.reject(`Got ${this.pageStatus} response from URL`);
                }
                let reviews = await this.getReviewsOfPage(page);
                this.DATA = this.DATA.concat(reviews);
            }catch(err) {
                // If we got error on a page, don't exit. Trying to fetch reviews on next pages.
                console.mylogger("ERROR", {err});
            }
            console.mylogger(`Reviews: ${this.DATA.length}/${this.totalReviewsCount} \nPages: ${pagenumber}/${this.totalPagesCount}`);
        }
        return this.DATA;
    }

}


module.exports = TigerdirectScraper;
