var convict = require('convict');

var config = convict({
    env: {
        doc: "The application environment.",
        format: ["production", "development", "test"],
        default: "development",
        env: "NODE_ENV",
        arg: "node_env"
    },
    pageNumber: {
        doc: "the page number from which we want to crawl all reviews.",
        format: "int",
        default: 0,
        env: "SCRAPE_PAGE_NUMBER",
        arg: "scrape_page_number"
    },
    reviewsPerPage: {
        doc: "total number reviews on a Page.",
        format: "int",
        default: 5,
        env: "REVIEWS_PER_PAGE",
        arg: "reviews_per_page"
    },
});

config.validate({allowed: 'strict'});

module.exports = config;
