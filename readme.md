# crawler-api

This app provides API to scrape product reviews of [tigerdirect.com](http://www.tigerdirect.com).

## Installation

Use the node package manager [npm](https://nodejs.org/en/) to install foobar.

```bash
npm install
```

## Usage
To start the application run command

```
npm start
```
In production env you can start using forever
```
npm install -g forever
forever start bin/www
```

By default, app runs on 300 port. You can opne [localhost:3000](http://localhost:3000/) and enter URL of the Product page of which you review you want and submit the form.
The app will fetch and show the review( JSON format) in the page.

Note: In case, a product doesn't have any reviews, then you will get a blank array.
``{"reviews":[],"count":0}``
